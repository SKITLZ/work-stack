"use strict";

var gulp = require("gulp");
var sass = require("gulp-sass");
var plumber = require('gulp-plumber');
var postcss = require("gulp-postcss");
var autoprefixer = require("autoprefixer");
var server = require("browser-sync").create();
var minify = require("gulp-csso");
var rename = require("gulp-rename");
var imagemin = require("gulp-imagemin");
var posthtml = require("gulp-posthtml");
var include = require("posthtml-include");
var run = require("run-sequence");
var del = require("del");
var uglify = require("gulp-uglify")

//Полная обработка scss и создание минифицированного css
gulp.task("style", function () {
  gulp.src("source/sass/style.scss")
  .pipe(plumber())
  .pipe(sass())
  .pipe(postcss([
    autoprefixer
  ]))
  .pipe(gulp.dest("build/css"))
  .pipe(server.stream())
  .pipe(minify())
  .pipe(rename("style.min.css"))
  .pipe(gulp.dest("build/css"));
});

//Минифицирует js
gulp.task("script", function () {
  gulp.src("source/js/script.js")
  .pipe(gulp.dest("build/js"))
  .pipe(uglify())
  .pipe(server.stream())
  .pipe(rename("script.min.js"))
  .pipe(gulp.dest("build/js"));
});

//Инклюдид в html указанные тегом <include></include> данные и копирование всех *.html в билд
gulp.task("html", function () {
  return gulp.src("source/*.html")
    .pipe(posthtml([
      include()
    ]))
    .pipe(gulp.dest("build"));
});

//Минификация изображений без потери качества (убираются мета-данные). Заменяются исходники, а не файлы в билде.
gulp.task("images", function () {
  return gulp.src("source/img/**/*.{png,jpg,svg}")
    .pipe(imagemin([
    imagemin.optipng({optimizationLevel: 3}),
    imagemin.jpegtran({progressive: true}),
    imagemin.svgo()
  ]))
  .pipe(gulp.dest("source/img"));
});

//Создание сервера и отлов изменений scss/html/js
gulp.task("serve", function () {
  server.init({
    server: "build/"
  });
  
  gulp.watch("source/sass/**/*.scss", ["style", server.reload]);
  gulp.watch("source/*.html", ["html", server.reload]);
  gulp.watch("source/js/*.js", ["script", server.reload]);
});

//Удаляет папку с билдом
gulp.task("clean", function () {
  return del("build");
});

//Копирует исходники в папку с билдом
gulp.task("copy", function () {
  return gulp.src ([
    "source/fonts/**",
    "source/img/**",
    "source/js/**",
    "source/css/*.css"
  ], {
    base: "source/"
  })
  .pipe(gulp.dest("build"));
});

//Создание билда одной задачей
gulp.task("build", function (done) {
  run("clean", "images", "copy", "style", "script", "html", done)
});
