## Project structure
    build/
        css/
        fonts/
        img/
        js/
        page.html
    node_modules/
    PSD/
        template.psd
        template.png
    source/
        fonts/
        img/
        js/
        sass/
            blocks/
            misc/
                fonts.scss
                scaffolding.scss
                variables.scss
            modals/
                modal.scss
                modal-name.scss
            style.scss
        page.html
    gulpfile.js
    package.json
    package-lock.json
    README.md
    .gitignore

## .gitignore
    node_modules/
    build/
    PSD/
    .vscode/
    README.md

*Добавить конфиг .vscode?*